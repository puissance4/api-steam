let express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    bodyParser = require('body-parser'),
    cors = require('cors');

app.listen(port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());


let routes = require('./api/routes/apiSteamRoute'); //importing route
routes(app); //register the route

console.log('ApiSteam started on: ' + port);
