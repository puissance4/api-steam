const SteamAPI = require('steamapi');
const steam = new SteamAPI('948DA74E440D0670AB87A94FF4171038');

'use strict';


exports.user_summary = function(req, res) {
    let steamid = req.query.steamid;
    if(steamid)
    {
        steam.getUserSummary(steamid).then(summary =>
        {
            res.json(summary);
        });
    }
    else
    {
        res.json(req.query);
    }
};

exports.user_friends = function(req, res) {
    let steamid = req.query.steamid;
    if(steamid)
    {
        steam.getUserFriends(steamid).then(friends =>
        {
            res.json(friends);
        });
    }
    else
    {
        res.json(req.query);
    }
};

exports.user_groups = function(req, res) {
    let steamid = req.query.steamid;
    if(steamid)
    {
        steam.getUserGroups(steamid).then(groups =>
        {
            res.json(groups);
        });
    }
    else
    {
        res.json(req.query);
    }
};

exports.user_owned_games = function(req, res) {
    let steamid = req.query.steamid;
    if(steamid)
    {
        steam.getUserOwnedGames(steamid).then(ownedGames =>
        {
            res.json(ownedGames);
        });
    }
    else
    {
        res.json(req.query);
    }
};

exports.user_recent_games = function(req, res) {
    let steamid = req.query.steamid;
    if(steamid)
    {
        steam.getUserRecentGames(steamid).then(recentGames =>
        {
            res.json(recentGames);
        });
    }
    else
    {
        res.json(req.query);
    }
};

exports.user_level = function(req, res) {
    let steamid = req.query.steamid;
    if(steamid)
    {
        steam.getUserLevel(steamid).then(level =>
        {
            res.json(level);
        });
    }
    else
    {
        res.json(req.query);
    }
};

exports.user_badges = function(req, res) {
    let steamid = req.query.steamid;
    if(steamid)
    {
        steam.getUserBadges(steamid).then(badges =>
        {
            res.json(badges);
        });
    }
    else
    {
        res.json(req.query);
    }
};

exports.user_bans = function(req, res) {
    let steamid = req.query.steamid;
    if(steamid)
    {
        steam.getUserBans(steamid).then(bans =>
        {
            res.json(bans);
        });
    }
    else
    {
        res.json(req.query);
    }
};

exports.featured_categories = function(req, res) {
    steam.getFeaturedCategories().then(featuredCategories =>
    {
        res.json(featuredCategories);
    });
};

exports.user_id = function(req, res) {
    let userUrl = req.query.userUrl;
    let steamid;
    if(userUrl)
    steam.resolve(userUrl).then(id =>
    {
        steamid=id;
        steam.getUserSummary(steamid).then(summary =>
            {
                res.json(summary);
            });
    });

};


