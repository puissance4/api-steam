'use strict';
module.exports = function(app) {
    let apiSteam = require('../controllers/apiSteamController');

    //apiSteam Routes
    app.route('/steam/userSummary/')
        .get(apiSteam.user_summary);

    app.route('/steam/userFriends/')
        .get(apiSteam.user_friends);

    app.route('/steam/userGroups/')
        .get(apiSteam.user_groups);

    app.route('/steam/userOwnedGames/')
        .get(apiSteam.user_owned_games);

    app.route('/steam/userRecentGames/')
        .get(apiSteam.user_recent_games);

    app.route('/steam/userLevel/')
        .get(apiSteam.user_level);

    app.route('/steam/userBadges/')
        .get(apiSteam.user_badges);

    app.route('/steam/userBans/')
        .get(apiSteam.user_bans);

    app.route('/steam/featuredCategories/')
        .get(apiSteam.featured_categories);

    app.route('/steam/userUrl/')
    .get(apiSteam.user_id);

};
